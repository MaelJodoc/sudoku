import sudoku.solver.algorithms.util.Cell;
import sudoku.solver.algorithms.util.SudokuDataObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class SudokuDataObjectTest {


    int[][] trueArray = {
            {0, 1, 2, 3, 4, 5, 6, 7, 8},
            {9, 0, 1, 2, 3, 4, 5, 6, 7},
            {8, 9, 0, 1, 2, 3, 4, 5, 6},

            {7, 8, 9, 0, 1, 2, 3, 4, 5},
            {6, 7, 8, 9, 0, 1, 2, 3, 4},
            {5, 6, 7, 8, 9, 0, 1, 2, 3},

            {4, 5, 6, 7, 8, 9, 0, 1, 2},
            {3, 4, 5, 6, 7, 8, 9, 0, 1},
            {2, 3, 4, 5, 6, 7, 8, 9, 0}
    };

    @Test
    public void getRowTest() {
        SudokuDataObject sdo = new SudokuDataObject(trueArray, false);
        Cell[] row = sdo.getRow(0);
        int[] intRow = new int[9];
        for (int i = 0; i < row.length; i++) {
            intRow[i] = row[i].getValue();
        }
        Assert.assertArrayEquals(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8}, intRow);
    }

    @Test
    public void getColumnTest() {
        SudokuDataObject sdo = new SudokuDataObject(trueArray, false);
        Cell[] column = sdo.getColumn(0);
        int[] intColumn = new int[9];
        for (int i = 0; i < column.length; i++) {
            intColumn[i] = column[i].getValue();
        }
        Assert.assertArrayEquals(new int[]{0, 9, 8, 7, 6, 5, 4, 3, 2}, intColumn);
    }

    @Test
    public void getQuadTest() {
        SudokuDataObject sdo = new SudokuDataObject(trueArray, false);
        Cell[] quad = sdo.getQuad(1, 3);
        int[] intQuad = new int[9];
        for (int i = 0; i < quad.length; i++) {
            intQuad[i] = quad[i].getValue();
        }
        Arrays.sort(intQuad);
        int[] expected = {3, 4, 5, 2, 3, 4, 1, 2, 3};
        Arrays.sort(expected);
        Assert.assertArrayEquals(expected, intQuad);
    }
}
