import sudoku.solver.algorithms.FindDoubles;
import sudoku.solver.algorithms.util.Cell;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.TreeSet;

public class FindDoublesTest {

    @Test
    public void updateForecastTest() throws Exception {
        Cell[] cells = new Cell[9];
        cells[0] = new Cell(0, new TreeSet<>(Arrays.asList(2, 3)));
        cells[1] = new Cell(0, new TreeSet<>(Arrays.asList(2, 3)));
        cells[2] = new Cell(0, new TreeSet<>(Arrays.asList(1, 2)));
        cells[3] = new Cell(0, new TreeSet<>(Arrays.asList(1, 2, 3, 4)));
        cells[4] = new Cell(0, new TreeSet<>(Arrays.asList(5, 6, 7, 8)));
        cells[5] = new Cell(0, new TreeSet<>(Arrays.asList(5, 6, 7, 8)));
        cells[6] = new Cell(0, new TreeSet<>(Arrays.asList(5, 6, 7, 8)));
        cells[7] = new Cell(0, new TreeSet<>(Arrays.asList(5, 6, 7, 8)));
        cells[8] = new Cell(0, new TreeSet<>(Arrays.asList(5, 6, 7, 8)));

        Cell[] expected = new Cell[9];
        expected[0] = new Cell(0, new TreeSet<>(Arrays.asList(2, 3)));
        expected[1] = new Cell(0, new TreeSet<>(Arrays.asList(2, 3)));
        expected[2] = new Cell(1, new TreeSet<>(Collections.emptyList()));
        expected[3] = new Cell(0, new TreeSet<>(Arrays.asList(1, 4)));
        expected[4] = new Cell(0, new TreeSet<>(Arrays.asList(5, 6, 7, 8)));
        expected[5] = new Cell(0, new TreeSet<>(Arrays.asList(5, 6, 7, 8)));
        expected[6] = new Cell(0, new TreeSet<>(Arrays.asList(5, 6, 7, 8)));
        expected[7] = new Cell(0, new TreeSet<>(Arrays.asList(5, 6, 7, 8)));
        expected[8] = new Cell(0, new TreeSet<>(Arrays.asList(5, 6, 7, 8)));

        TreeSet<Integer> forecast = new TreeSet<>(Arrays.asList(2, 3));
        //Немножко темной магии (не хочу делать метод публичным, поэтому только рефлексия)
        Method updateForecast = FindDoubles.class.getDeclaredMethod("updateForecast", Cell[].class, TreeSet.class);
        updateForecast.setAccessible(true);
        boolean updated = (boolean) updateForecast.invoke(null, cells, forecast);
        if (!updated) Assert.fail();

        for (int i = 0; i < cells.length; i++) {
            Cell cell = cells[i];
            Cell exp = expected[i];
            if (cell.getValue() != exp.getValue()) Assert.fail();
            if (!cell.getForecast().equals(exp.getForecast())) Assert.fail();
        }
    }
}
