import sudoku.solver.algorithms.BaseRuleControl;
import sudoku.solver.algorithms.util.SudokuDataObject;
import org.junit.Assert;
import org.junit.Test;

public class BaseRuleControlTest {
    int[][] task = {
            {6, 5, 0, 2, 4, 0, 0, 9, 0},
            {8, 9, 0, 0, 1, 3, 0, 0, 0},
            {0, 1, 2, 0, 9, 7, 0, 4, 8},

            {9, 0, 0, 1, 2, 0, 8, 0, 7},
            {7, 3, 6, 4, 8, 0, 0, 0, 0},
            {0, 0, 1, 0, 0, 0, 5, 6, 4},

            {0, 0, 9, 8, 0, 2, 1, 7, 0},
            {5, 0, 0, 9, 0, 1, 4, 0, 3},
            {1, 2, 3, 0, 5, 4, 0, 8, 0}
    };

    int[][] solution = {
            {6, 5, 7, 2, 4, 8, 3, 9, 1},
            {8, 9, 4, 6, 1, 3, 7, 5, 2},
            {3, 1, 2, 5, 9, 7, 6, 4, 8},
            {9, 4, 5, 1, 2, 6, 8, 3, 7},
            {7, 3, 6, 4, 8, 5, 2, 1, 9},
            {2, 8, 1, 3, 7, 9, 5, 6, 4},
            {4, 6, 9, 8, 3, 2, 1, 7, 5},
            {5, 7, 8, 9, 6, 1, 4, 2, 3},
            {1, 2, 3, 7, 5, 4, 9, 8, 6}
    };

    @Test
    public void canSolveIdiotTaskTest() {
        SudokuDataObject sdo = new SudokuDataObject(task);
        BaseRuleControl baseRuleControl = new BaseRuleControl(sdo);
        baseRuleControl.execute();
        baseRuleControl.execute();
        baseRuleControl.execute();
        int[][] actual = sdo.getFieldAsIntArray();
        Assert.assertArrayEquals(solution, actual);
    }
}
