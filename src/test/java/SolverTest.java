import sudoku.solver.Solver;
import sudoku.solver.algorithms.util.TaskChecker;
import org.junit.Assert;
import org.junit.Test;

public class SolverTest {
    public int[][] task1 = {
            {9, 4, 5, 0, 0, 0, 0, 0, 2},
            {0, 0, 0, 6, 4, 0, 3, 0, 9},
            {0, 1, 6, 0, 0, 0, 8, 5, 0},
            {4, 0, 8, 0, 0, 0, 9, 7, 5},
            {6, 0, 0, 9, 0, 4, 0, 8, 0},
            {2, 0, 9, 3, 0, 8, 0, 4, 0},
            {0, 0, 2, 0, 6, 7, 0, 0, 0},
            {7, 6, 0, 4, 0, 0, 5, 0, 8},
            {0, 0, 0, 5, 3, 0, 6, 9, 7}
    };

    public int[][] task2 = {
            {0, 0, 0, 4, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 6, 8, 0, 5, 0},
            {7, 6, 2, 3, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 7, 6},
            {1, 0, 0, 5, 4, 9, 0, 0, 0},
            {8, 0, 5, 6, 0, 0, 0, 0, 1},
            {2, 0, 1, 0, 0, 4, 0, 0, 0},
            {0, 0, 0, 0, 5, 7, 0, 0, 2},
            {0, 8, 0, 0, 1, 0, 0, 3, 5}
    };

    public int[][] task3 = {
            {0, 0, 5, 3, 0, 0, 0, 0, 0},
            {8, 0, 0, 0, 0, 0, 0, 2, 0},
            {0, 7, 0, 0, 1, 0, 5, 0, 0},
            {4, 0, 0, 0, 0, 5, 3, 0, 0},
            {0, 1, 0, 0, 7, 0, 0, 0, 6},
            {0, 0, 3, 2, 0, 0, 0, 8, 0},
            {0, 6, 0, 5, 0, 0, 0, 0, 9},
            {0, 0, 4, 0, 0, 0, 0, 3, 0},
            {0, 0, 0, 0, 0, 9, 7, 0, 0}
    };

    public int[][] task4 = new int[9][9];

    public int[][] task5 = {
            {0, 0, 7, 0, 0, 0, 9, 0, 0},
            {0, 0, 0, 0, 1, 0, 4, 7, 2},
            {0, 0, 4, 0, 8, 0, 0, 0, 0},
            {0, 6, 0, 9, 0, 0, 0, 1, 0},
            {0, 0, 0, 0, 0, 0, 0, 3, 0},
            {0, 9, 0, 6, 0, 3, 0, 0, 0},
            {1, 0, 0, 7, 0, 8, 0, 0, 0},
            {7, 0, 0, 0, 0, 6, 0, 0, 0},
            {3, 0, 0, 0, 0, 0, 0, 8, 5}

    };

    public int[][] task6 = {
            {8, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 3, 6, 0, 0, 0, 0, 0},
            {0, 7, 0, 0, 9, 0, 2, 0, 0},
            {0, 5, 0, 0, 0, 7, 0, 0, 0},
            {0, 0, 0, 0, 4, 5, 7, 0, 0},
            {0, 0, 0, 1, 0, 0, 0, 3, 0},
            {0, 0, 1, 0, 0, 0, 0, 6, 8},
            {0, 0, 8, 5, 0, 0, 0, 1, 0},
            {0, 9, 0, 0, 0, 0, 4, 0, 0}
    };

    private static boolean checkResult(int[][] result) {
        for (int[] ints : result) {
            for (int i : ints) {
                if (i == 0) return false;
            }
        }
        return TaskChecker.isCorrect(result);
    }

    @Test
    public void solveTest1() {
        int[][] result = Solver.solve(task1);
        Assert.assertTrue(checkResult(result));
    }
    @Test
    public void solveTest2() {
        int[][] result = Solver.solve(task2);
        Assert.assertTrue(checkResult(result));
    }
    @Test
    public void solveTest3() {
        int[][] result = Solver.solve(task3);
        Assert.assertTrue(checkResult(result));
    }
    @Test
    public void solveTest4() {
        int[][] result = Solver.solve(task4);
        Assert.assertTrue(checkResult(result));
    }
    @Test
    public void solveTest5() {
        int[][] result = Solver.solve(task5);
        Assert.assertTrue(checkResult(result));
    }
    @Test
    public void solveTest6() {
        int[][] result = Solver.solve(task6);
        Assert.assertTrue(checkResult(result));
    }
}
