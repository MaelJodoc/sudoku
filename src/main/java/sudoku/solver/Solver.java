package sudoku.solver;

import sudoku.solver.algorithms.*;
import sudoku.solver.algorithms.util.SudokuDataObject;


public class Solver {

    public static int[][] solve(int[][] task) {
        SudokuDataObject sudokuDataObject = new SudokuDataObject(task);
        Algorithm baseRuleControl = new BaseRuleControl(sudokuDataObject);
        Algorithm findDoubles = new FindDoubles(sudokuDataObject);
        Algorithm recursion = new Recursion(sudokuDataObject);
        Algorithm excessForecast = new ExcessForecast(sudokuDataObject);

        while (true) {
            SudokuDataObject sdo;
            do {
                baseRuleControl.execute();
            } while (baseRuleControl.isUpdated());

            findDoubles.execute();
            if (findDoubles.isUpdated()) continue;

            excessForecast.execute();
            if (excessForecast.isUpdated()) continue;

            return recursion.execute().getFieldAsIntArray();
        }
    }
}
