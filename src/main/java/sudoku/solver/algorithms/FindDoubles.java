package sudoku.solver.algorithms;

import sudoku.solver.algorithms.util.Cell;
import sudoku.solver.algorithms.util.SudokuDataObject;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class FindDoubles extends Algorithm {
    public FindDoubles(SudokuDataObject sudokuDataObject) {
        super(sudokuDataObject);
    }

    @Override
    public SudokuDataObject execute() {
        super.updated = false;
        Cell[][] field = sudokuDataObject.getField();
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                Cell cell = field[i][j];
                if (cell.getValue() == 0) {
                    TreeSet<Integer> forecast = cell.getForecast();
                    Cell[] row = sudokuDataObject.getRow(i);
                    super.updated |= updateForecast(row, forecast);
                    Cell[] column = sudokuDataObject.getColumn(j);
                    super.updated |= updateForecast(column, forecast);
                    Cell[] quad = sudokuDataObject.getQuad(i, j);
                    super.updated |= updateForecast(quad, forecast);
                }
            }
        }
        return super.sudokuDataObject;
    }

    private static boolean updateForecast(Cell[] cells, TreeSet<Integer> forecast) {
        boolean updated = false;
        List<Cell> cellsWithEqualsForecast = new ArrayList<>();
        for (Cell c : cells) {
            if (c.getValue() == 0 && c.getForecast().equals(forecast)) cellsWithEqualsForecast.add(c);
        }
        if (forecast.size() == cellsWithEqualsForecast.size()) {
            for (Cell c : cells) {
                if (c.getValue() == 0 && !cellsWithEqualsForecast.contains(c)) {
                    TreeSet<Integer> currentCellForecast = c.getForecast();
                    updated |= currentCellForecast.removeAll(forecast);
                    if (currentCellForecast.size() == 1) {
                        c.setValue(currentCellForecast.pollFirst());
                    }
                }
            }
        }
        return updated;
    }
}
