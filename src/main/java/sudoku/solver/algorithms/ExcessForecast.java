package sudoku.solver.algorithms;

import sudoku.solver.algorithms.util.Cell;
import sudoku.solver.algorithms.util.SudokuDataObject;

import java.util.TreeSet;

public class ExcessForecast extends Algorithm {
    public ExcessForecast(SudokuDataObject sudokuDataObject) {
        super(sudokuDataObject);
    }

    @Override
    public SudokuDataObject execute() {
        super.updated = false;
        Cell[][] field = sudokuDataObject.getField();
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                Cell cell = field[i][j];
                if (cell.getValue() == 0) {
                    Cell[] row = sudokuDataObject.getRow(i);
                    super.updated |= updateForecast(row, cell);
                    if (super.updated) return super.sudokuDataObject;
                    Cell[] column = sudokuDataObject.getColumn(j);
                    super.updated |= updateForecast(column, cell);
                    if (super.updated) return super.sudokuDataObject;
                    Cell[] quad = sudokuDataObject.getQuad(i, j);
                    super.updated |= updateForecast(quad, cell);
                    if (super.updated) return super.sudokuDataObject;
                }
            }
        }
        return super.sudokuDataObject;
    }

    private static boolean updateForecast(Cell[] cells, Cell current) {
        for (Integer f : current.getForecast()) {
            boolean isForecastValuePresentInOther = false;
            for (Cell c : cells) {
                if (c == current) continue; //да, сравнение именно по ссылке.
                if (c.getValue() != 0) continue;
                if (c.getForecast().contains(f)) {
                    isForecastValuePresentInOther = true;
                    break;
                }
            }
            if (!isForecastValuePresentInOther) {
                current.setValue(f);
                current.setForecast(new TreeSet<>());
                return true;
            }
        }
        return false;
    }
}
