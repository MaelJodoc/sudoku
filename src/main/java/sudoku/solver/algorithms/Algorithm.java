package sudoku.solver.algorithms;

import sudoku.solver.algorithms.util.SudokuDataObject;

abstract public class Algorithm {
    protected SudokuDataObject sudokuDataObject;
    protected boolean updated;

    public Algorithm(SudokuDataObject sudokuDataObject) {
        this.sudokuDataObject = sudokuDataObject;
    }

    public abstract SudokuDataObject execute();

    public boolean isUpdated() {
        return updated;
    }

    public SudokuDataObject getSudokuDataObject() {
        return sudokuDataObject;
    }
}
