package sudoku.solver.algorithms;

import sudoku.solver.algorithms.util.Cell;
import sudoku.solver.algorithms.util.SudokuDataObject;

import java.util.TreeSet;

public class BaseRuleControl extends Algorithm {
    public BaseRuleControl(SudokuDataObject sudokuDataObject) {
        super(sudokuDataObject);
    }

    @Override
    public SudokuDataObject execute() {
        super.updated = false;
        Cell[][] field = sudokuDataObject.getField();
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                Cell cell = field[i][j];
                if (cell.getValue() == 0) {
                    TreeSet<Integer> forecast = cell.getForecast();
                    Cell[] row = sudokuDataObject.getRow(i);
                    Cell[] column = sudokuDataObject.getColumn(j);
                    Cell[] quad = sudokuDataObject.getQuad(i, j);
                    for (Cell c : row) super.updated |= forecast.remove(c.getValue());
                    for (Cell c : column) super.updated |= forecast.remove(c.getValue());
                    for (Cell c : quad) super.updated |= forecast.remove(c.getValue());
                    if (forecast.size() == 1) {
                        cell.setValue(forecast.pollFirst());
                    }
                }
            }
        }
        return super.sudokuDataObject;
    }
}
