package sudoku.solver.algorithms;

import sudoku.solver.algorithms.util.Cell;
import sudoku.solver.algorithms.util.SudokuDataObject;

import java.util.*;

public class Recursion extends Algorithm {
    public Recursion(SudokuDataObject sudokuDataObject) {
        super(sudokuDataObject);
    }

    @Override
    public SudokuDataObject execute() {
        if (solved(super.sudokuDataObject)) return super.sudokuDataObject;
        return execute(super.sudokuDataObject);
    }

    private SudokuDataObject execute(SudokuDataObject sdo) {
        List<SudokuDataObject> variantList = new ArrayList<>();
        int rowIndex = 0;
        int columnIndex = 0;
        int minForecastSize = Integer.MAX_VALUE;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (sdo.getField()[i][j].getForecast().size() > 0 && sdo.getField()[i][j].getForecast().size() < minForecastSize) {
                    minForecastSize = sdo.getField()[i][j].getForecast().size();
                    rowIndex = i;
                    columnIndex = j;
                }
            }
        }
        if (minForecastSize == Integer.MAX_VALUE) return null; //гадать бесполезно, где-то ошиблись
        TreeSet<Integer> forecast = sdo.getField()[rowIndex][columnIndex].getForecast();
        for (Integer i : forecast) {
            SudokuDataObject s = deepCopy(sdo);
            Cell cell = s.getField()[rowIndex][columnIndex];
            cell.setValue(i);
            cell.setForecast(new TreeSet<>());
            variantList.add(s);
        }

        for (SudokuDataObject s : variantList) {
            BaseRuleControl baseRuleControl = new BaseRuleControl(s);
            FindDoubles findDoubles = new FindDoubles(s);
            ExcessForecast excessForecast = new ExcessForecast(s);
            while (true) {
                do {
                    baseRuleControl.execute();
                } while (baseRuleControl.isUpdated());

                findDoubles.execute();
                if (findDoubles.isUpdated()) continue;

                excessForecast.execute();
                if (excessForecast.isUpdated()) continue;
                if (solved(s)) {
                    return s;
                }

                SudokuDataObject result = this.execute(s);
                if (result == null) break; //если ошиблись, рассматриваем другой вариант
                else{
                    return result;
                }
            }
        }
        return null;
    }

    private SudokuDataObject deepCopy(SudokuDataObject original) {
        return new SudokuDataObject(original);
    }

    private boolean solved(SudokuDataObject sdo) {
        int[][] fieldAsIntArray = sdo.getFieldAsIntArray();
        for (int[] ints : fieldAsIntArray) {
            for (int i : ints) {
                if (i == 0) return false;
            }
        }
        return true;
    }
}
