package sudoku.solver.algorithms.util;

import java.util.Comparator;
import java.util.TreeSet;

public class Cell {
    private int value; //0 if undefine. Cell is empty
    private TreeSet<Integer> forecast;

    public Cell(int value, TreeSet<Integer> forecast) {
        this.value = value;
        this.forecast = forecast;
    }

    public Cell(Cell original) {
        this.value = original.value;
        this.forecast = new TreeSet<>(Comparator.naturalOrder());
        for (Integer i : original.forecast) {
            int v = i;
            this.forecast.add(v);
        }
    }

    public int getValue() {
        return value;
    }

    public TreeSet<Integer> getForecast() {
        return forecast;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setForecast(TreeSet<Integer> forecast) {
        this.forecast = forecast;
    }
}
