package sudoku.solver.algorithms.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskChecker {
   public static boolean isCorrect(int[][] task) {
        if (task.length != 9) return false;
        for (int i = 0; i < 9; i++) {
            if (task[i].length != 9) return false;
        }

        for (int i = 0; i < 9; i++) {
            if (!isCorrect(getRow(i, task))) return false;
            if (!isCorrect(getColumn(i, task))) return false;
        }

        for (int i = 0; i < 9; i += 3) {
            for (int j = 0; j < 9; j += 3) {
                if (!isCorrect(getQuad(i, j, task))) return false;
            }
        }
        return true;
    }

    private static int[] getRow(int rowIndex, int[][] task) {
        return task[rowIndex];
    }

    private static int[] getColumn(int columnIndex, int[][] task) {
        int[] column = new int[9];
        for (int i = 0; i < 9; i++) {
            column[i] = task[i][columnIndex];
        }
        return column;
    }

    private static int[] getQuad(int rowIndex, int columnIndex, int[][] task) {
        int ri = (rowIndex / 3) * 3;
        int ci = (columnIndex / 3) * 3;
        List<Integer> quad = new ArrayList<>(9);
        for (int i = ri; i < ri + 3; i++) {
            for (int j = ci; j < ci + 3; j++) {
                quad.add(task[i][j]);
            }
        }
        int[] result = new int[9];
        for (int i = 0; i < result.length; i++) {
            result[i] = quad.get(i);
        }
        return result;
    }

    private static boolean isCorrect(int[] mas) {
        int[] copy = Arrays.copyOf(mas, mas.length);
        Arrays.sort(copy);
        if (copy[0] < 0) return false;
        if (copy[8] > 9) return false;
        for (int i = 0; i < copy.length - 1; i++) {
            if (copy[i] == copy[i + 1] && copy[i] != 0) return false;
        }
        return true;
    }
}
