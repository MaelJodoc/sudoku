package sudoku.solver.algorithms.util;

import java.util.*;

public class SudokuDataObject {
    private Cell[][] field;

    public SudokuDataObject(int[][] task, boolean needCheckTask) {
        this.field = new Cell[9][9];
        if (needCheckTask) {
            boolean correct = TaskChecker.isCorrect(task);
            if (!correct) throw new RuntimeException();
        }

        for (int i = 0; i < task.length; i++) {
            for (int j = 0; j < task[0].length; j++) {
                int value = task[i][j];
                field[i][j] = new Cell(value, new TreeSet<>(Comparator.naturalOrder()));
                if (value == 0) {
                    field[i][j].getForecast().addAll(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
                }
            }
        }
    }

    public SudokuDataObject(int[][] task) {
        this(task, true);
    }

    public SudokuDataObject(SudokuDataObject original) {
        Cell[][] originalField = original.getField();
        this.field = new Cell[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                Cell orig = originalField[i][j];
                Cell copy = new Cell(orig);
                this.field[i][j] = copy;
            }
        }
    }

    public Cell[][] getField() {
        return field;
    }


    public int[][] getFieldAsIntArray() {
        int[][] result = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                result[i][j] = field[i][j].getValue();
            }
        }
        return result;
    }

    public Cell[] getRow(int rowIndex) {
        return field[rowIndex];
    }

    public Cell[] getColumn(int columnIndex) {
        Cell[] column = new Cell[9];
        for (int i = 0; i < 9; i++) {
            column[i] = field[i][columnIndex];
        }
        return column;
    }

    //возврашает ячейки квадрата 3х3 в который попадает наша ячейка
    public Cell[] getQuad(int rowIndex, int columnIndex) {
        int ri = (rowIndex / 3) * 3;
        int ci = (columnIndex / 3) * 3;
        List<Cell> quad = new ArrayList<>(9);
        for (int i = ri; i < ri + 3; i++) {
            quad.addAll(Arrays.asList(field[i]).subList(ci, ci + 3));
        }
        return quad.toArray(new Cell[9]);
    }
}
