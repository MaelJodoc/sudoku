package sudoku.controller.Exceptions;

public class TaskDataException extends Exception {
    public TaskDataException() {
    }

    public TaskDataException(String message) {
        super(message);
    }

    public TaskDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskDataException(Throwable cause) {
        super(cause);
    }

    public TaskDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
