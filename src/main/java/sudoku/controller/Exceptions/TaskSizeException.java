package sudoku.controller.Exceptions;

public class TaskSizeException extends Exception {
    public TaskSizeException() {
    }

    public TaskSizeException(String message) {
        super(message);
    }

    public TaskSizeException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskSizeException(Throwable cause) {
        super(cause);
    }

    public TaskSizeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
