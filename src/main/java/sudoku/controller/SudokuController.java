package sudoku.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sudoku.controller.Exceptions.TaskDataException;
import sudoku.controller.Exceptions.TaskSizeException;
import sudoku.solver.Solver;
import sudoku.solver.algorithms.util.TaskChecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


@RestController
@RequestMapping("/sudoku")
public class SudokuController {
    @GetMapping
    public String info() {
        return "This is sudoku solver!";
    }

    @PostMapping("/solveArr")
    public ResponseEntity<int[][]> solveArr(@RequestBody String[][] task) {
        try {
            int[][] ints = taskToIntArr(task);
            int[][] result = Solver.solve(ints);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (TaskSizeException | TaskDataException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/solveFile")
    public ResponseEntity<int[][]> solveFile(@RequestParam("file") MultipartFile file) {
        String[][] task = new String[9][9];
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            int currentLineNumber = 0;
            while (reader.ready()) {
                String s = reader.readLine().replaceAll(" ", "");
                if (s.isEmpty()) continue;
                if (currentLineNumber == 9) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                if (s.length() != 9) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                char[] chars = s.toCharArray();
                for (int i = 0; i < chars.length; i++) {
                    task[currentLineNumber][i] = Character.toString(chars[i]);
                }
                currentLineNumber++;
            }
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return solveArr(task);
    }

    private static int[][] taskToIntArr(String[][] task) throws TaskSizeException, TaskDataException {
        if (task.length != 9) throw new TaskSizeException();
        for (int i = 0; i < 9; i++) {
            if (task[i].length != 9) throw new TaskSizeException();
        }
        int[][] result = new int[9][9];
        for (int i = 0; i < task.length; i++) {
            for (int j = 0; j < task[0].length; j++) {
                if (task[i][j].equals("*")) {
                    result[i][j] = 0;
                } else {
                    try {
                        int current = Integer.parseInt(task[i][j]);
                        result[i][j] = current;
                    } catch (NumberFormatException e) {
                        throw new TaskDataException();
                    }
                }
            }
        }
        if (!TaskChecker.isCorrect(result)) throw new TaskDataException();
        return result;
    }
}
